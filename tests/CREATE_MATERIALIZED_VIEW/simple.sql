-- relkind 22 -- IntoClause.rel.RangeVar.relpersistence = p
CREATE MATERIALIZED VIEW sales_summary AS
SELECT
  seller_no,
  invoice_date,
  sum(invoice_amt) :: numeric(13, 2) as sales_amt
FROM invoice
WHERE invoice_date < CURRENT_DATE
GROUP BY seller_no, invoice_date
ORDER BY seller_no, invoice_date;
